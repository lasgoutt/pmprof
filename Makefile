# This should be a C++11 compiler
CXX = g++
COMPILE = $(CXX) -std=c++11 -O2 -Wall -Wextra
PROGS = testpmprof

all: $(PROGS)

clean:
	rm -rf $(PROGS) *~

testpmprof: testpmprof.cpp pmprof.h
	$(COMPILE) $< -o $@
