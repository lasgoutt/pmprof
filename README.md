Pmprof: a poor man's profiler for C++
=====================================

Pmprof is a one-header profiler for C++11 that allows to measure the
time taken by any particular block of code.

Why use such a trivial profiler?
--------------------------------

* Because it is very easy: just one `#include` and one macro and you are set.

* Because this is portable to any C++11 code: you can send a patch to
  any other developer running macOS, Windows or whatever other OS and
  get back a result that you understand.

* Because getting a real profiler running is sometimes excruciating.


How to use this profiler?
-------------------------

1. Copy the file `pmprof.h` in your code directory and `#include` it.

2. At the beginning of the block of interest, just add a `PROFILE_THIS_BLOCK` macro call:

   ```c++
   for (int i = 0 ; i < 100 ; ++i) {
   	PROFILE_THIS_BLOCK(usleep_100ms);
   	std::this_thread::sleep_for(std::chrono::milliseconds(100));
   }
   ```

   A trailing semicolon can be added at your discretion.

3. When the program ends, statistics will be printed on standard error, like:

    ```
    #pmprof# usleep_100ms: 100.11 ms, count=100, total=10.01 s
    ```

4. if `DISABLE_PMPROF` is defined before including `pmprof.h`, the
  profiler is replaced by empty macros. This is useful for keeping the
  instrumentation in place without any overhead.


Profiling a cache
-----------------

It is also possible to profile caching schemes. All it takes is an additional
```c++
PROFILE_CACHE_MISS(some_identifier)
```
in the place that takes care of cache misses. Then the output at
the end will change to something like

```
#pmprof# some_identifier: 6.51usec, count=7120, total=46.33msec
   hit: 96%, 4.36usec, count=6849, total=29.89msec
  miss: 3%, 60.65usec, count=271, total=16.43msec
```

About profiling scope
---------------------

The code measured by the profiler corresponds to the lifetime of a
local variable declared by the `PROFILE_THIS_BLOCK` macro.

Some examples of profiling scope: In the snippets below, `c1`, `c2`...
designate code chunks, and the identifiers of profiling blocks are
chosen to reflect what they count.
```c++
{
  c1
  PROFILE_THIS_BLOCK(c2)
  c2
}
```

```c++
{
  PROFILE_THIS_BLOCK(c1_c2)
  c1
  PROFILE_THIS_BLOCK(c2)
  c2
}
```

```c++
{
  {
    PROFILE_THIS_BLOCK(c1)
    c1
  }
  PROFILE_THIS_BLOCK(c2)
  c2
}
```

```c++
{
  PROFILE_THIS_BLOCK(c1_c2_c3)
  c1
  {
    PROFILE_THIS_BLOCK(c2)
    c2
  }
  c3
}
```

Influence of identifier names: they are mainly used for display
purpose, but the same name should not be used twice in the same
scope.

```c++
{
  PROFILE_THIS_BLOCK(foo)
  c1
  PROFILE_THIS_BLOCK(foo) // error: identifier clash
  c2
}
```

In the example below, `c1 + c2` and `c2` are counted separately, but in
the output, both are confusingly labelled `foo`.

```c++
{
  PROFILE_THIS_BLOCK(foo)
  c1
  {
    PROFILE_THIS_BLOCK(foo) // no error but same name
    c2
  }
}
```

