// -*- C++ -*-
/* \file pmprof.h
 *
 * Copyright (c) 2013-2022 Jean-Marc Lasgouttes
 * https://gitlab.inria.fr/lasgoutt/pmprof
 */


#ifndef PMPROF_H
#define PMPROF_H

#if defined(DISABLE_PMPROF)

// Make pmprof an empty shell
#define PROFILE_THIS_BLOCK(a)
#define PROFILE_CACHE_MISS(a)

#else

#include <chrono>
#include <iomanip>
#include <iostream>


#if defined(__GNUG__) && defined(_GLIBCXX_DEBUG)
#error Profiling is not usable when run-time debugging is in effect
#endif

namespace _pmprof {

using namespace std;
using namespace chrono;

namespace {

void dump_time(system_clock::duration value)
{
	auto musec = duration_cast<microseconds>(value).count();
	cerr << fixed << setprecision(2);
	if (musec >= 1000000)
		cerr << musec / 1000000.0 << " s";
	else if (musec >= 1000)
		cerr << musec / 1000.0 << " ms";
	else
		cerr << musec << " us";
}

void dump(system_clock::duration total, unsigned long long count) {
	if (count > 0) {
		dump_time(total / count);
		cerr << ", count=" << count << ", total=";
		dump_time(total);
	} else
		std::cerr << "no data";
	cerr << endl;
}

} // namespace


/* Helper class for gathering data. Instantiate this as a static
 * variable, so that its destructor will be executed when the program
 * ends.
 */
class stat {
public:
	stat(char const * name) : name_(name) {}

	~stat() {
		if (count_ + miss_count_ > 0) {
			if (miss_count_ == 0) {
				cerr << "#pmprof# " << name_ << ": ";
				dump(dur_, count_);
			}
			else {
				cerr << "#pmprof# " << name_ << ": ";
				dump(dur_ + miss_dur_, count_ + miss_count_);
				cerr << "   hit: " << 100 * count_ / (count_ + miss_count_) << "%, ";
				dump(dur_, count_);
				cerr << "  miss: " << 100 * miss_count_ / (count_ + miss_count_) << "%, ";
				dump(miss_dur_, miss_count_);
			}
		} else
			std::cerr << "#pmprof# " << name_ << ": no data" << std::endl;
	}

	void add(system_clock::duration d, const bool hit) {
		if (hit) {
			dur_ += d;
			count_++;
		} else {
			miss_dur_ += d;
			miss_count_++;
		}
	}

private:
	char const * name_;
	system_clock::duration dur_, miss_dur_;
	unsigned long long count_ = 0, miss_count_ = 0;
};


/* Helper class which gathers data at the end of the scope. One
 * instance of this one should be created at each execution of the
 * block. At the end of the block, it sends statistics to the static
 * stat object.
 */
class instance {
public:
	instance(stat * stat) : hit(true), stat_(stat)
	{
		before_ = system_clock::now();
	}

	~instance() {
		stat_->add(system_clock::now() - before_, hit);
	}

	bool hit;

private:
	system_clock::time_point before_;
	stat * stat_;
};

}

#define PROFILE_THIS_BLOCK(a) \
	static _pmprof::stat _pmps_##a(#a);	\
	_pmprof::instance _pmpi_##a(&_pmps_##a);

#define PROFILE_CACHE_MISS(a) \
	_pmpi_##a.hit = false;

#endif // !defined(DISABLE_PMPROF)

#endif
