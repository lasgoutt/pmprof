/**
 * Demo file for pmprof.h
 */

#include <unistd.h>

#include "pmprof.h"

#include <thread>

using namespace std;

int main(int, char **)
{
	PROFILE_THIS_BLOCK(everything_10s);
	for (int i = 0 ; i < 100 ; ++i) {
		PROFILE_THIS_BLOCK(usleep_100ms);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	return 0;
}
